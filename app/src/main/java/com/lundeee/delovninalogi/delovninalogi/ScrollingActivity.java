package com.lundeee.delovninalogi.delovninalogi;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.zj.btsdk.BluetoothService;
import com.zj.btsdk.PrintPic;


import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import static android.widget.Toast.*;

public class ScrollingActivity extends AppCompatActivity {
    private boolean isStarted = false;
    private EditText mStrankaText,mTelefonText;

    @SuppressLint("StaticFieldLeak")
    public static TextView mNalogText;
    private TextView mViewDate;
    private Button btnA1, btnA2, btnA3, btnA4, btnA5, btnA6, btnA7, btnA8;
    private Button btnS1, btnS2, btnS3, btnS4, btnS5, btnS6, btnS7, btnS8;
    private NestedScrollView scrollView;

    private Button btnPrint,btnClear;
    private String date;
    private Calendar now;
    private Integer charCount;
    private Integer maxCharr = 32;
    private SimpleDateFormat format1 = new SimpleDateFormat("dd.MMM.yyyy HH:mm",Locale.US);

    private static final int REQUEST_ENABLE_BT = 2;
    BluetoothService mService = null;
    BluetoothDevice con_dev = null;
    private static final int REQUEST_CONNECT_DEVICE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scrolling);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        scrollView = (NestedScrollView) findViewById(R.id.scrollView);
        mService = new BluetoothService(this, mHandler);


        if(!mService.isAvailable()){
            makeText(this, "Bluetooth is not available", LENGTH_LONG).show();
            finish();
        }


        mStrankaText = (EditText) findViewById(R.id.strankaText);
        mTelefonText = (EditText) findViewById(R.id.telefonText);
        mNalogText = (TextView) findViewById(R.id.nalogText);
        mViewDate = (TextView) findViewById(R.id.dateViev);


        btnA1 = (Button) findViewById(R.id.btn1);
        btnA2 = (Button) findViewById(R.id.btn2);
        btnA3 = (Button) findViewById(R.id.btn3);
        btnA4 = (Button) findViewById(R.id.btn4);
        btnA5 = (Button) findViewById(R.id.btn5);
        btnA6 = (Button) findViewById(R.id.btn6);
        btnA7 = (Button) findViewById(R.id.btn7);
        btnA8 = (Button) findViewById(R.id.btn8);

        btnS1 = (Button) findViewById(R.id.btn9);
        btnS2 = (Button) findViewById(R.id.btn10);
        btnS3 = (Button) findViewById(R.id.btn11);
        btnS4 = (Button) findViewById(R.id.btn12);
        btnS5 = (Button) findViewById(R.id.btn13);
        btnS6 = (Button) findViewById(R.id.btn14);
        btnS7 = (Button) findViewById(R.id.btn15);
        btnS8 = (Button) findViewById(R.id.btn16);


        btnPrint = (Button) findViewById(R.id.btn17);
        btnClear = (Button) findViewById(R.id.btn20);

        //set date
        now = Calendar.getInstance();
        date = format1.format(now.getTime());
        mViewDate.setText(date);

        //set button callbacks
        mNalogText.setOnClickListener(onClickText());

        btnA1.setOnClickListener(onClickA());
        btnA2.setOnClickListener(onClickA());
        btnA3.setOnClickListener(onClickA());
        btnA4.setOnClickListener(onClickA());
        btnA5.setOnClickListener(onClickA());
        btnA6.setOnClickListener(onClickA());
        btnA7.setOnClickListener(onClickA());
        btnA8.setOnClickListener(onClickA());

        btnS1.setOnClickListener(onClickB());
        btnS2.setOnClickListener(onClickB());
        btnS3.setOnClickListener(onClickB());
        btnS4.setOnClickListener(onClickB());
        btnS5.setOnClickListener(onClickB());
        btnS6.setOnClickListener(onClickB());
        btnS7.setOnClickListener(onClickB());
        btnS8.setOnClickListener(onClickB());

        btnS1.setEnabled(false);
        btnS2.setEnabled(false);
        btnS3.setEnabled(false);
        btnS4.setEnabled(false);
        btnS5.setEnabled(false);
        btnS6.setEnabled(false);
        btnS7.setEnabled(false);
        btnS8.setEnabled(false);

        btnPrint.setEnabled(false);

        btnPrint.setOnClickListener(onClickPrint());
        btnClear.setOnClickListener(onClickClear());

        mTelefonText.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE){
                    scrollView.scrollTo(0, scrollView.getHeight());
                    hideSoftKeyboard();

                }
                return true;

            }
                                               }

        );



        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                makeText(ScrollingActivity.this, "Nov nalog", LENGTH_LONG).show();

                now = Calendar.getInstance();
                date = format1.format(now.getTime());
                mViewDate.setText(date);

                mNalogText.setText("");
                mStrankaText.setText("");
                mTelefonText.setText("");
                btnS1.setEnabled(false);
                btnS2.setEnabled(false);
                btnS3.setEnabled(false);
                btnS4.setEnabled(false);
                btnS5.setEnabled(false);
                btnS6.setEnabled(false);
                btnS7.setEnabled(false);
                btnS8.setEnabled(false);

                btnA1.setEnabled(true);
                btnA2.setEnabled(true);
                btnA3.setEnabled(true);
                btnA4.setEnabled(true);
                btnA5.setEnabled(true);
                btnA6.setEnabled(true);
                btnA7.setEnabled(true);
                btnA8.setEnabled(true);

            }
        });
    }

    public void hideSoftKeyboard() {
        if(getCurrentFocus()!=null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    @Override
    public void onStart() {
        super.onStart();


        if(!mService.isBTopen())
        {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_scrolling, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_connect){
            Intent serverIntent = new Intent(ScrollingActivity.this, DeviceListActivity.class);
            startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
        }
        if (id == R.id.action_clear) {
            makeText(ScrollingActivity.this, "Nov nalog", LENGTH_LONG).show();
            now = Calendar.getInstance();
            date = format1.format(now.getTime());
            mViewDate.setText(date);

            mNalogText.setText("");
            mStrankaText.setText("");
            mTelefonText.setText("");
            btnS1.setEnabled(false);
            btnS2.setEnabled(false);
            btnS3.setEnabled(false);
            btnS4.setEnabled(false);
            btnS5.setEnabled(false);
            btnS6.setEnabled(false);
            btnS7.setEnabled(false);
            btnS8.setEnabled(false);

            btnA1.setEnabled(true);
            btnA2.setEnabled(true);
            btnA3.setEnabled(true);
            btnA4.setEnabled(true);
            btnA5.setEnabled(true);
            btnA6.setEnabled(true);
            btnA7.setEnabled(true);
            btnA8.setEnabled(true);
        }
        if (id == R.id.action_print) {
            printNalog();
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        menu.findItem(R.id.action_print).setVisible(isStarted);

        return true;
    }


    private View.OnClickListener onClickClear() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int last  = mNalogText.getText().toString().lastIndexOf("\n");
                if (last > 0) {
                   last =  mNalogText.getText().toString().substring(0,last).lastIndexOf("\n");

                }
                if (last > 0) {
                    String text = mNalogText.getText().toString().substring(0, last)+"\n";
                    mNalogText.setText(text);
                }
                else {
                    mNalogText.setText("");
                }
                btnS1.setEnabled(false);
                btnS2.setEnabled(false);
                btnS3.setEnabled(false);
                btnS4.setEnabled(false);
                btnS5.setEnabled(false);
                btnS6.setEnabled(false);
                btnS7.setEnabled(false);
                btnS8.setEnabled(false);

                btnA1.setEnabled(true);
                btnA2.setEnabled(true);
                btnA3.setEnabled(true);
                btnA4.setEnabled(true);
                btnA5.setEnabled(true);
                btnA6.setEnabled(true);
                btnA7.setEnabled(true);
                btnA8.setEnabled(true);


            }
        };
    }
    private View.OnClickListener onClickText() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Intent serverIntent = new Intent(ScrollingActivity.this, Edit_Text.class);
                startActivity(serverIntent);

            }
        };
    }
    private View.OnClickListener onClickA() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mNalogText.append(((Button) view).getTag().toString());

                charCount = ((Button) view).getTag().toString().length();

                btnA1.setEnabled(false);
                btnA2.setEnabled(false);
                btnA3.setEnabled(false);
                btnA4.setEnabled(false);
                btnA5.setEnabled(false);
                btnA6.setEnabled(false);
                btnA7.setEnabled(false);
                btnA8.setEnabled(false);

                btnS1.setEnabled(true);
                btnS2.setEnabled(true);
                btnS3.setEnabled(true);
                btnS4.setEnabled(true);
                btnS5.setEnabled(true);
                btnS6.setEnabled(true);
                btnS7.setEnabled(true);
                btnS8.setEnabled(true);

            }
        };
    }
    private View.OnClickListener onClickB() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Integer n = maxCharr - charCount - ((Button) view).getTag().toString().length()-2;

                String fillN = new String(new char[n]).replace("\0", ".");
                mNalogText.append(" "+fillN+" ");
                mNalogText.append(((Button) view).getTag().toString() +"\n");



                btnA1.setEnabled(true);
                btnA2.setEnabled(true);
                btnA3.setEnabled(true);
                btnA4.setEnabled(true);
                btnA5.setEnabled(true);
                btnA6.setEnabled(true);
                btnA7.setEnabled(true);
                btnA8.setEnabled(true);


                charCount = 0;
                scrollView.scrollTo(0, scrollView.getHeight());
            }
        };
    }
    private View.OnClickListener onClickPrint() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                printNalog();

            }
        };
    }
    private void printNalog(){

        byte[] center = new byte[]{ 0x1b, 0x61, 0x01 };
        byte[] format = { 27, 33, 0 };
        mService.write(center);

        String msg = getString(R.string.header);
        mService.sendMessage(msg, "GBK");

        mService.write(format);

        msg=mStrankaText.getText().toString()+"\n";
        if(mTelefonText.getText().toString()!=""){
        msg=msg+"tel: "+mTelefonText.getText().toString()+"\n\n";};
        msg=msg+mViewDate.getText().toString()+"\n";
        msg=msg+getString(R.string.crta);
        msg=msg+mNalogText.getText().toString();
        msg=msg+getString(R.string.crta);
        msg=msg+getString(R.string.footer);


        if (msg.length() > 0) {
            mService.sendMessage(msg + "\n", "GBK");
        }

    }

    private final  Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case BluetoothService.MESSAGE_STATE_CHANGE:
                    switch (msg.arg1) {
                        case BluetoothService.STATE_CONNECTED:
                            Toast.makeText(getApplicationContext(), "Connect successful",
                                    Toast.LENGTH_SHORT).show();
                            //btnClose.setEnabled(true);
                            btnPrint.setEnabled(true);
                            isStarted=true;
                            //btnSendDraw.setEnabled(true);
                            break;
                        case BluetoothService.STATE_CONNECTING:
                            Log.d("STATE_CONNECTING","jknkn.....");
                            break;
                        case BluetoothService.STATE_LISTEN:
                        case BluetoothService.STATE_NONE:
                            Log.d("STATE_NONE","lklkn.....");
                            break;
                    }
                    break;
                case BluetoothService.MESSAGE_CONNECTION_LOST:
                    Toast.makeText(getApplicationContext(), "Device connection was lost",
                            Toast.LENGTH_SHORT).show();
                    //btnClose.setEnabled(false);
                    btnPrint.setEnabled(false);
                    isStarted=false;
                    //btnSendDraw.setEnabled(false);
                    break;
                case BluetoothService.MESSAGE_UNABLE_CONNECT:
                    Toast.makeText(getApplicationContext(), "Unable to connect device",
                            Toast.LENGTH_SHORT).show();
                    break;
            }
        }

    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_ENABLE_BT:
                if (resultCode == Activity.RESULT_OK) {
                    Toast.makeText(this, "Bluetooth open successful", Toast.LENGTH_LONG).show();
                } else {
                    finish();
                }
                break;
            case  REQUEST_CONNECT_DEVICE:
                if (resultCode == Activity.RESULT_OK) {
                    String address = data.getExtras()
                            .getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
                    con_dev = mService.getDevByMac(address);

                    mService.connect(con_dev);
                }
                break;
        }
    }


    @SuppressLint("SdCardPath")
    private void printImage() {
        byte[] sendData = null;
        PrintPic pg = new PrintPic();
        pg.initCanvas(384);
        pg.initPaint();
        pg.drawImage(0, 0, "/mnt/sdcard/icon.jpg");
        sendData = pg.printDraw();
        mService.write(sendData);
    }
}
