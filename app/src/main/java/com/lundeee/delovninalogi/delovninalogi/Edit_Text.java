package com.lundeee.delovninalogi.delovninalogi;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class Edit_Text extends Activity {
    private EditText mEditText;
    private Button mFinishButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_text);

        mEditText=(EditText) findViewById(R.id.editText);
        mFinishButton=(Button) findViewById(R.id.finishButton);
        mEditText.setText(ScrollingActivity.mNalogText.getText().toString());

        mFinishButton.setOnClickListener(applyText());
    }
    private View.OnClickListener applyText() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ScrollingActivity.mNalogText.setText(mEditText.getText().toString());
                finish();

            }
        };
    }
}
